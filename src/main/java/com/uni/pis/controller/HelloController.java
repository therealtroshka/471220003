package com.uni.pis.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;

@RestController
public class HelloController {

    @GetMapping("/")
    public String index() {
        return "Greetings from Spring Boot!";
    }

    @GetMapping("/random-greeting")
    public String getRandomGreeting() {
        int r = new Random().nextInt(3);
        switch (r) {
            case 0 -> {
                return "Random Greeting 1";
            }
            case 1 -> {
                return "Random Greeting 2";
            }
            case 2 -> {
                return "Random Greeting 3";
            }
            default -> {
                return "Default Greeting";
            }
        }
    }
}